package dat.ptit.wibu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import dat.ptit.wibu.Adapter.ViewPageAdapter;

public class CMessageActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        viewPager=findViewById(R.id.view_page);
        bottomNavigationView=findViewById(R.id.bottom_navigation);
        ViewPageAdapter viewPagerAdapter=new ViewPageAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(viewPagerAdapter);
        bottomNavigationView=findViewById(R.id.bottom_navigation);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
//                    case 0:
//                        bottomNavigationView.getMenu().findItem(R.id.message).setChecked(true);
//                        break;
                    case  0:
                        bottomNavigationView.getMenu().findItem(R.id.friend).setChecked(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
//                    case R.id.message:
//                        viewPager.setCurrentItem(0);
//                        break;
                    case R.id.friend:
                        viewPager.setCurrentItem(0);
                        break;

                }
                return true;
            }
        });
    }
}