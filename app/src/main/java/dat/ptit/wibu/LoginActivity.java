package dat.ptit.wibu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private EditText edtEmail,edtPw;
    private TextView txtFpw,txtRegister;
    private Button btnLogin;
    private FirebaseAuth auth;
    private FirebaseUser firebaseUser;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseUser =FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser!=null){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        txtRegister.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
            startActivity(intent);
            //chuyển hướng đổi activity
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
        });
        btnLogin.setOnClickListener(v -> {
            ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Vui lòng chờ...");
            dialog.show();
            String str_email = edtEmail.getText().toString();
            String str_password = edtPw.getText().toString();

            if(TextUtils.isEmpty(str_email)||TextUtils.isEmpty(str_password)){
                Toast.makeText(LoginActivity.this, "Nhập đầy đủ email và password", Toast.LENGTH_SHORT).show();
            }else {
                auth.signInWithEmailAndPassword(str_email,str_password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("User")
                                            .child(auth.getCurrentUser().getUid());
                                    reference.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            dialog.dismiss();
                                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.slide_in_dow,R.anim.slide_out_dow);
                                            finish();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {
                                            dialog.dismiss();
                                        }
                                    });
                                }else {
                                    dialog.dismiss();
                                    Toast.makeText(LoginActivity.this, "Đăng nhập thất bại!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

    }

    private void init(){
        edtEmail=findViewById(R.id.edtEmail);
        edtPw=findViewById(R.id.edtPassword);
        txtFpw=findViewById(R.id.txtFpw);
        txtRegister=findViewById(R.id.txtRegister);
        btnLogin=findViewById(R.id.btnLogin);
        auth=FirebaseAuth.getInstance();

    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        LoginActivity.super.onBackPressed();
                    }
                }).create().show();
    }
}