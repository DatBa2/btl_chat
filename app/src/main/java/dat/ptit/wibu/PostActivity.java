package dat.ptit.wibu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.HashMap;

public class PostActivity extends AppCompatActivity {
    Uri imageUrl;
    String url="";
    StorageTask uploadTask;
    StorageReference storageReference;
    private ImageButton close;
    private ImageView image_added;
    private TextView txtShare;
    private EditText desrciption;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        init();
        storageReference = FirebaseStorage.getInstance().getReference("posts");
        close.setOnClickListener(v -> {
            new AlertDialog.Builder(this)
                    .setTitle("Really Cancel ?")
                    .setMessage("Bạn có muốn hủy bài viết?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            startActivity(new Intent(PostActivity.this,MainActivity.class));
                            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                            finish();
                        }
                    }).create().show();
        });
        txtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
        CropImage.activity().setAspectRatio(1,1)
                .start(PostActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode==RESULT_OK){
            CropImage.ActivityResult activityResult = CropImage.getActivityResult(data);
            imageUrl = activityResult.getUri();

            image_added.setImageURI(imageUrl);
        }else {
//            Toast.makeText(this, "Tải ảnh thất bại!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(PostActivity.this,MainActivity.class));
            finish();
        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap =MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadImage(){
        ProgressDialog dialog=new ProgressDialog(this);
        dialog.setMessage("Posting");
        dialog.show();

        if(imageUrl != null){
            StorageReference reference = storageReference.child(System.currentTimeMillis()
                +"."+getFileExtension(imageUrl));

            uploadTask = reference.putFile(imageUrl);
            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {
                    if(!task.isSuccessful()){
                        throw task.getException();

                    }
                    return reference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        Uri downloadUri =  task.getResult();
                        url = downloadUri.toString();

                        DatabaseReference referenceDB= FirebaseDatabase.getInstance().getReference("Posts");
                        String postid = referenceDB.push().getKey();
                        HashMap<String,Object> hashMap= new HashMap<>();
                        hashMap.put("postid",postid);
                        hashMap.put("postimage",url);
                        hashMap.put("description",desrciption.getText().toString());
                        hashMap.put("publiser", FirebaseAuth.getInstance().getCurrentUser().getUid());
                        referenceDB.child(postid).setValue(hashMap);
                        dialog.dismiss();
                        startActivity(new Intent(PostActivity.this,MainActivity.class));
                        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_left);
                        finish();
                    }else {
                        Toast.makeText(PostActivity.this, "Thất bại!", Toast.LENGTH_SHORT).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(PostActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(this, "Chưa chọn ảnh", Toast.LENGTH_SHORT).show();
        }
    }

    private void init(){
        close=findViewById(R.id.btn_close);
        image_added=findViewById(R.id.image_added);
        txtShare=findViewById(R.id.txtShare);
        desrciption=findViewById(R.id.edt_description);
    }
}