package dat.ptit.wibu.Model;

public class Chat {
    private  String sender,reciver,message;
    private boolean isseen;

    public Chat() {
    }

    public Chat(String sender, String receiver, String message, boolean isseen) {
        this.sender = sender;
        this.reciver = reciver;
        this.message = message;
        this.isseen = isseen;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciver() {
        return reciver;
    }

    public void setReciver(String reciver) {
        this.reciver = reciver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "sender='" + sender + '\'' +
                ", reciver='" + reciver + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isIsseen() {
        return isseen;
    }

    public void setIsseen(boolean isseen) {
        this.isseen = isseen;
    }
}
