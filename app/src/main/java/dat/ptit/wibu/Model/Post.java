package dat.ptit.wibu.Model;

public class Post {
    private String postid;
    private String description;
    private String postimage;
    private String publiser;

    public Post() {
    }

    public Post(String postid, String description, String postimage, String publiser) {
        this.postid = postid;
        this.description = description;
        this.postimage = postimage;
        this.publiser = publiser;
    }

    @Override
    public String toString() {
        return "Post{" +
                "postid='" + postid + '\'' +
                ", description='" + description + '\'' +
                ", postimage='" + postimage + '\'' +
                ", publiser='" + publiser + '\'' +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPostimage() {
        return postimage;
    }

    public void setPostimage(String postimage) {
        this.postimage = postimage;
    }

    public String getPubliser() {
        return publiser;
    }

    public void setPubliser(String publiser) {
        this.publiser = publiser;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }
}
