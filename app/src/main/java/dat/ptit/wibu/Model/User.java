package dat.ptit.wibu.Model;

public class User {
    private String  id;
    private String username;
    private String nickname;
    private String imageurl;
    private String birth;
    private String life;


    public String getNickname() {
        return nickname;
    }

    public User(String id, String username, String nickname, String imageurl, String birth, String life) {
        this.id = id;
        this.username = username;
        this.nickname = nickname;
        this.imageurl = imageurl;
        this.birth = birth;
        this.life = life;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public User() {
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }
}
