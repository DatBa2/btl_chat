package dat.ptit.wibu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private EditText edtEmail,edtUsername,edtPw,edtFPw;
    private Button btnRegister;
    private ImageButton btnBackLogin;
    private TextView txtLogin,txtCheckEmail,txtCheckPassword,txtCheckFPassword,txtCheckUserName;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Boolean validateEmail = false;
    private Boolean validateUser = false;
    private Boolean validatePasswork = false;
    private Boolean validateFPassword = false;
    private FirebaseAuth auth;
    DatabaseReference reference;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        txtLogin.setOnClickListener(v -> {
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            //chuyển hướng đổi activity
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
            //finish();
        });
        validate();
        btnRegister.setOnClickListener(v -> {
            //Toast.makeText(getApplicationContext(), edtPw.getText().toString()+"   "+edtFPw.getText().toString(), Toast.LENGTH_SHORT).show();
            if(validateEmail&&validateFPassword&&validatePasswork&&validateUser){
                //Register
                dialog=new ProgressDialog(RegisterActivity.this);
                dialog.setMessage("Vui lòng chờ ...");
                dialog.show();
                String str_userName =edtUsername.getText().toString();
                String str_email =edtEmail.getText().toString();
                String str_password =edtUsername.getText().toString();
                register(str_email,str_userName,str_password);
            }else {
                Toast.makeText(RegisterActivity.this, "Thông tin chưa hơp lệ", Toast.LENGTH_SHORT).show();
            }
        });
        btnBackLogin.setOnClickListener(v->{
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            //chuyển hướng đổi activity
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
            finish();
        });
    }

    private void register(String str_email, String str_userName, String str_password) {
        auth.createUserWithEmailAndPassword(str_email,str_password).
                addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser firebaseUser =auth.getCurrentUser();
                            String userid=firebaseUser.getUid();
                            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(userid);
                            HashMap<String,Object> hashMap =new HashMap<>();
                            hashMap.put("id",userid);
                            hashMap.put("username",str_userName);
                            hashMap.put("birth","");
                            hashMap.put("nickname","");
                            hashMap.put("imageurl","https://firebasestorage.googleapis.com/v0/b/wibu-dd9b3.appspot.com/o/icon.png?alt=media&token=34b1b412-4570-42cd-968d-13012a26a7a3");
                            hashMap.put("life","");
                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        dialog.dismiss();
                                        Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
                            });

                        }else {
                            dialog.dismiss();
                            Toast.makeText(RegisterActivity.this, "Đăng ký thất bại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void validate() {
        checkEmail();
        checkPassword();
        checkFPasswork();
        checkUserName();
    }

    private void checkUserName() {
        edtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtUsername.getText().toString().length()>0){
                    txtCheckUserName.setVisibility(View.INVISIBLE);
                    validateUser=true;
                }else {
                    txtCheckUserName.setVisibility(View.VISIBLE);
                    validateUser=false;
                }
            }
        });
    }

    private void checkFPasswork() {
        edtFPw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(edtPw.getText().toString().trim().equals(edtFPw.getText().toString().trim())){

                    txtCheckFPassword.setVisibility(View.INVISIBLE);
                    validateFPassword=true;
                }
                else
                {
                    txtCheckFPassword.setVisibility(View.VISIBLE);
                    validateFPassword=false;
                }
            }
        });
    }

    private void checkPassword() {
        edtPw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtPw.getText().toString().length()>=6)
                {
                    txtCheckPassword.setVisibility(View.INVISIBLE);
                    validatePasswork=true;
                }
                else
                {

                    txtCheckPassword.setVisibility(View.VISIBLE);
                    validatePasswork=false;
                }
            }
        });
    }

    private void checkEmail() {
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtEmail.getText().toString().matches(emailPattern) && s.length() > 0)
                {
                    txtCheckEmail.setVisibility(View.INVISIBLE);
                    validateEmail=true;
                }
                else
                {

                    txtCheckEmail.setVisibility(View.VISIBLE);
                    validateEmail=false;
                }
            }
        });
    }


    private void init(){
        edtEmail=findViewById(R.id.edtEmail);
        edtUsername=findViewById(R.id.edtUsername);
        edtPw=findViewById(R.id.edtPassword);
        edtFPw=findViewById(R.id.edtETPassword);
        btnRegister=findViewById(R.id.btnRegister);
        txtLogin=findViewById(R.id.txtLogin);
        txtCheckEmail=findViewById(R.id.txtCheckEmail);
        txtCheckPassword=findViewById(R.id.txtCheckPassword);
        txtCheckFPassword=findViewById(R.id.txtCheckFPassword);
        txtCheckUserName=findViewById(R.id.txtCheckUserName);
        btnBackLogin=findViewById(R.id.btn_backlogin);
        auth=FirebaseAuth.getInstance();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
    }
}