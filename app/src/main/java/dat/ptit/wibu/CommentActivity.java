package dat.ptit.wibu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dat.ptit.wibu.Adapter.CommentAdapter;
import dat.ptit.wibu.Model.Comment;
import dat.ptit.wibu.Model.User;

public class CommentActivity extends AppCompatActivity {
     EditText edtAddComment;
     CircleImageView image_profile;
     ImageButton btnSendCmt;
     String postId;
     String publisherid;
     FirebaseUser firebaseUser;
     private RecyclerView recyclerView;
     private CommentAdapter commentAdapter;
     private List<Comment>  comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        init();
        Intent intent =getIntent();
        postId = intent.getStringExtra("postid");
        publisherid=intent.getStringExtra("publisherid");
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        comments = new ArrayList<>();
        commentAdapter = new CommentAdapter(this,comments);
        recyclerView.setAdapter(commentAdapter);
        btnSendCmt.setOnClickListener(v->{
            if(edtAddComment.getText().toString().equals("")){
                Toast.makeText(CommentActivity.this, "Nhập comment", Toast.LENGTH_SHORT).show();
            }else {
                actionAddComment();
            }
        });

        getImage();
        readComment();
    }

    private void actionAddComment() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Comments").child(postId);

        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("comment",edtAddComment.getText().toString().trim());
        hashMap.put("publisher",firebaseUser.getUid());
        reference.push().setValue(hashMap);
        addNotifications();
        edtAddComment.setText("");
    }

    private void getImage(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user= snapshot.getValue(User.class);
                Glide.with(getApplicationContext()).load(user.getImageurl()).into(image_profile);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void   addNotifications(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications").child(publisherid);
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("userid",firebaseUser.getUid());
        hashMap.put("text","Đã bình luân:"+edtAddComment.getText().toString());
        hashMap.put("postid",postId);
        hashMap.put("ispost",true);
        reference.push().setValue(hashMap);
    }

    private void init(){

        btnSendCmt=findViewById(R.id.btnAddCmt);
        image_profile=findViewById(R.id.image_profile);
        edtAddComment=findViewById(R.id.edt_comment);
        firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
        recyclerView=findViewById(R.id.recycler_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Comments");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v->{
            finish();
        });
    }

    private void readComment(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Comments").child(postId);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                comments.clear();
                for (DataSnapshot dataSnapshot :snapshot.getChildren()){
                    Comment comment = dataSnapshot.getValue(Comment.class);
                    comments.add(comment);
                }
                commentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}