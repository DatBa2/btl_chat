package dat.ptit.wibu.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dat.ptit.wibu.Adapter.MyFotoAdapter;
import dat.ptit.wibu.EditProfileActivity;
import dat.ptit.wibu.FollowertsActivity;
import dat.ptit.wibu.Model.Post;
import dat.ptit.wibu.Model.User;
import dat.ptit.wibu.OptionsActivity;
import dat.ptit.wibu.R;


public class ProfileFragment extends Fragment {

    TextView userName,posts,following,followers,nickName;
    CircleImageView image_profile;
    ImageView optisons;
    Button edti_profile;

    RecyclerView  recyclerView;
    MyFotoAdapter myFotoAdapter;
    List<Post> postList;

    FirebaseUser firebaseUser;
    String profileid;
    ImageButton my_fostos,saved_fotos;

    RecyclerView  recyclerView_saved;
    List<Post> list_post_saved;
    MyFotoAdapter myFotoAdapter_saved;


    List<String> my_save;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_profile,container,false);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences preferences = getContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        profileid = preferences.getString("profileid","none");

        image_profile = view.findViewById(R.id.image_profile);
        optisons = view.findViewById(R.id.options);
        userName = view.findViewById(R.id.username);
        posts = view.findViewById(R.id.posts);
        following = view.findViewById(R.id.following);
        followers = view.findViewById(R.id.followers);
        edti_profile = view.findViewById(R.id.edit_profile);
        my_fostos = view.findViewById(R.id.my_fotos);
        saved_fotos = view.findViewById(R.id.save_fotos);
        nickName = view.findViewById(R.id.txtNickName);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(),3);
        recyclerView.setLayoutManager(linearLayoutManager);
        postList = new ArrayList<>();
        myFotoAdapter = new MyFotoAdapter(getContext(),postList);
        recyclerView.setAdapter(myFotoAdapter);

        recyclerView_saved = view.findViewById(R.id.recycler_view_save);
        recyclerView_saved.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager_save = new GridLayoutManager(getContext(),3);
        recyclerView_saved.setLayoutManager(linearLayoutManager_save);
        list_post_saved = new ArrayList<>();
        myFotoAdapter_saved = new MyFotoAdapter(getContext(),list_post_saved);
        recyclerView_saved.setAdapter(myFotoAdapter_saved);

        recyclerView.setVisibility(View.VISIBLE);
        recyclerView_saved.setVisibility(View.GONE);

        userInfo();
        getFollowers();
        getFollowing();
        getPosts();
        myFotos();
        get_post_saved();
        read_post_save();
        if (profileid.equals(firebaseUser.getUid())){
            edti_profile.setText("Chỉnh sửa trang cá nhân");
            my_fostos.setMinimumWidth(555);
            saved_fotos.setMinimumWidth(555);
        }else {
            checkFollow();
            saved_fotos.setVisibility(View.GONE);
            my_fostos.setMinimumWidth(1100);

        }
        optisons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent    = new Intent(getContext(), OptionsActivity.class);
                startActivity(intent);
            }
        });



        edti_profile.setOnClickListener(v->{
            String btn = edti_profile.getText().toString();
            if (btn.equals("Chỉnh sửa trang cá nhân")){
                startActivity(new Intent(getContext(), EditProfileActivity.class));
            }else if (btn.equals("follow")){
                FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid()).child("following").child(profileid).setValue(true);
                FirebaseDatabase.getInstance().getReference().child("Follow").child(profileid).child("follower").child(firebaseUser.getUid()).setValue(true);
                addNotifications();
            }else if(btn.equals("following")){
                FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid()).child("following").child(profileid).removeValue();
                FirebaseDatabase.getInstance().getReference().child("Follow").child(profileid).child("follower").child(firebaseUser.getUid()).removeValue();
            }
        });

        saved_fotos.setOnClickListener(v -> {
            recyclerView.setVisibility(View.GONE);
            recyclerView_saved.setVisibility(View.VISIBLE);
        });

        my_fostos.setOnClickListener(v -> {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView_saved.setVisibility(View.GONE);
        });

        followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent    = new Intent(getContext(), FollowertsActivity.class    );
                intent.putExtra("id",profileid);
                intent.putExtra("key","followers");
                intent.putExtra("title","Người theo dõi");
                startActivity(intent);
            }
        });

        following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent    = new Intent(getContext(), FollowertsActivity.class    );
                intent.putExtra("id",profileid);
                intent.putExtra("key","following");
                intent.putExtra("title","Đang theo dõi");
                startActivity(intent);
            }
        });

        return view;
    }

    private void   addNotifications(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Notifications").child(profileid);
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("userid",firebaseUser.getUid());
        hashMap.put("text","Đã theo dõi bạn!");
        hashMap.put("postid","");
        hashMap.put("ispost",false);
        reference.push().setValue(hashMap);
    }


    private void userInfo(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child(profileid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(getContext() == null){
                    return;
                }

                User user = snapshot.getValue(User.class);
                Glide.with(getContext()).load(user.getImageurl()).into(image_profile);
                userName.setText(user.getUsername());
                nickName.setText(user.getNickname());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void checkFollow(){
        DatabaseReference reference= FirebaseDatabase.getInstance().getReference().child("Follow").child(firebaseUser.getUid()).child("following");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.child(profileid).exists()){
                    edti_profile.setText("following");
                }else {
                    edti_profile.setText("follow");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getFollowers(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Follow").child(profileid).child("follower");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                followers.setText(""+snapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getFollowing(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference()
                .child("Follow").child(profileid).child("following");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                following.setText(""+snapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    private void getPosts(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int i = 0 ;
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Post post = dataSnapshot.getValue(Post.class);
                    if (post.getPubliser().equals(profileid)){
                        i++;
                    }
                }

                posts.setText(""+i);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void myFotos(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Posts");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                postList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Post post = dataSnapshot.getValue(Post.class);
                    if(post.getPubliser().equals(profileid)){
                        postList.add(post);
                    }
                }
                //đảo ngược lại danh sách bài post

                Collections.reverse(postList);
                myFotoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void get_post_saved(){
        my_save = new ArrayList<>();
        DatabaseReference reference =FirebaseDatabase.getInstance().getReference("Saves").child(firebaseUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                my_save.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    my_save.add(dataSnapshot.getKey());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    private void read_post_save(){
        DatabaseReference reference =FirebaseDatabase.getInstance().getReference("Posts");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list_post_saved.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Post  post = dataSnapshot.getValue(Post.class);
                    for (String id :my_save){
                        if (post.getPostid().equals(id)){
                            list_post_saved.add(post);
                        }
                    }
                }
                myFotoAdapter_saved.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}