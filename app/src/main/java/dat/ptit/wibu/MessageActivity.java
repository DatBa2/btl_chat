package dat.ptit.wibu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import dat.ptit.wibu.Adapter.MessageAdapter;
import dat.ptit.wibu.Model.Chat;
import dat.ptit.wibu.Model.User;

public class MessageActivity extends AppCompatActivity {
    private CircleImageView profile_image;
    private TextView username;
    private ImageButton btn_send;
    private EditText textSend;

    MessageAdapter messageAdapter;
    ArrayList<Chat> mChat;
    RecyclerView recyclerView;

    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    Intent intent;

    ValueEventListener seenListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_c);

        Toolbar toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");


        recyclerView =findViewById(R.id.recyler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        profile_image =findViewById(R.id.profile_image);
        username=findViewById(R.id.username);
        btn_send=findViewById(R.id.btn_send_mess);
        textSend=findViewById(R.id.text_send);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        intent =getIntent();
        String iduser=intent.getStringExtra("iduser");

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg=textSend.getText().toString();
                if(!msg.equals("")){
                    sendMessage(firebaseUser.getUid(),iduser,msg);
                }else {
                    Toast.makeText(MessageActivity.this, "Nhập tin nhắn đi nào !", Toast.LENGTH_SHORT).show();
                }textSend.setText("");
            }
        });
        databaseReference= FirebaseDatabase.getInstance().getReference("Users").child(iduser);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user=snapshot.getValue(User.class  );
                username.setText(user.getUsername());

                Glide.with(getApplicationContext()).load(user.getImageurl()).into(profile_image);
                readMessage(firebaseUser.getUid(),iduser,user.getImageurl());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void sendMessage(String sender,String reciver,String message){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        HashMap <String,Object> hashMap=new HashMap<>();
        hashMap.put("sender",sender);
        hashMap.put("reciver",reciver);
        hashMap.put("message",message);
        hashMap.put("isseen",false);

        databaseReference.child("Chats").push().setValue(hashMap);

    }


    private  void readMessage(String myid,String userid,String image){
        mChat =new ArrayList<>();

        databaseReference=FirebaseDatabase.getInstance().getReference("Chats");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mChat.clear();
                for (DataSnapshot dataSnapshot:snapshot.getChildren()){
                    Chat chat =dataSnapshot.getValue(Chat.class );
                    if (chat.getReciver().equals(myid) && chat.getSender().equals(userid) ||
                            chat.getReciver().equals(userid) && chat.getSender().equals(myid)) {
                        mChat.add(chat);
                    }


                }
                messageAdapter = new MessageAdapter(getApplicationContext(), mChat, image);
                recyclerView.setAdapter(messageAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}