package dat.ptit.wibu.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import dat.ptit.wibu.Fragment.FriendFragment;
import dat.ptit.wibu.Fragment.MessageFragment;

public class ViewPageAdapter extends FragmentPagerAdapter {
    public ViewPageAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
//            case 0:
//                return new MessageFragment();
            case 0:
                return new FriendFragment();
            default:
                return new FriendFragment();
        }
    }

    @Override
    public int getCount() {
        return 1;
    }
}
