package dat.ptit.wibu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import dat.ptit.wibu.Model.Chat;
import dat.ptit.wibu.Model.User;
import dat.ptit.wibu.R;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder>{
    private Context mcontext;
    private ArrayList<Chat> mChat;
    public static  final int MSG_TYPE_LEFT=0;
    public static  final  int MSG_TYPE_RIGHT=1;
    private String imageURL;
    FirebaseUser firebaseUser;

    public MessageAdapter(Context mcontext, ArrayList<Chat> mChat, String imageURL) {
        this.mcontext = mcontext;
        this.mChat   = mChat;
        this.imageURL =imageURL;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==MSG_TYPE_RIGHT){
            View view = LayoutInflater.from(mcontext).inflate(R.layout.itemright,parent,false);

            return new ViewHolder(view);
        }else {
            View view = LayoutInflater.from(mcontext).inflate(R.layout.itemleft,parent,false);

            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Chat chat = mChat.get(position);
        holder.show_message.setText(chat.getMessage());
        Glide.with(mcontext).load(imageURL).into(holder.profile_image);



    }

    @Override
    public int getItemCount() {
        return mChat.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{
        private TextView show_message;
        private CircleImageView profile_image;




        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            show_message=itemView.findViewById(R.id.show_message);
            profile_image=itemView.findViewById(R.id.profile_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(mChat.get(position).getSender().equals(firebaseUser.getUid())){
            return  MSG_TYPE_RIGHT;
        }
        else {
            return MSG_TYPE_LEFT;
        }
    }

}
