package dat.ptit.wibu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dat.ptit.wibu.MainActivity;
import dat.ptit.wibu.Model.Comment;
import dat.ptit.wibu.Model.User;
import dat.ptit.wibu.R;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{
    private Context context;
    private List<Comment> comments;
    private FirebaseUser firebaseUser;

    public CommentAdapter(Context context, List<Comment> comments) {
        this.context = context;
        this.comments = comments;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.comment_item,parent,false);
        return new CommentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Comment comment = comments.get(position);
        holder.txtComment.setText(comment.getComment());
        getUserInfo(holder.image_profile,holder.txtUsername,comment.getPublisher());
        holder.txtComment.setOnClickListener(v->{
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("publisher",comment.getPublisher());
            context.startActivity(intent );
        });
        holder.image_profile.setOnClickListener(v->{
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("publisher",comment.getPublisher());
            context.startActivity(intent );
        });

    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public CircleImageView image_profile;
        public TextView txtUsername,txtComment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image_profile=itemView.findViewById(R.id.image_profile);
            txtUsername=itemView.findViewById(R.id.txtUserName);
            txtComment=itemView.findViewById(R.id.txtComment);
        }
    }

    private void getUserInfo(CircleImageView image_profile,TextView username,String publisherid){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Users").child(publisherid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                Glide.with(context).load(user.getImageurl()).into(image_profile);
                username.setText(user.getUsername());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
