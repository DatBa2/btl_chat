package dat.ptit.wibu.Adapter;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dat.ptit.wibu.MessageActivity;
import dat.ptit.wibu.Model.Chat;
import dat.ptit.wibu.Model.User;
import dat.ptit.wibu.R;

public class UserMesAdapter extends RecyclerView.Adapter<UserMesAdapter.ViewHolder>{
    private Context context;
    private List<User> users;
    String lastMes;
    public UserMesAdapter(Context context, List<User> users ) {
        this.context = context;
        this.users = users;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item_mes,parent,false);
        return new UserMesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user=users.get(position);
        holder.username.setText(user.getUsername());
        Glide.with(context).load(user.getImageurl()).into(holder.profile_image);



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MessageActivity.class);
                intent.putExtra("iduser",user.getId());
                context.startActivity(intent);
//                Toast.makeText(context, "test", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView username;
        private CircleImageView profile_image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            username=itemView.findViewById(R.id.username);
            profile_image=itemView.findViewById(R.id.profile_image);

        }
    }

//    private void lastMessage(String userid,TextView laTextView){
//        lastMes="default";
//        FirebaseUser firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
//        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                for(DataSnapshot dataSnapshot :snapshot.getChildren()){
//                    Chat chat = dataSnapshot.getValue(Chat.class);
//                    try {
//                        if(chat.getReciver().equals(firebaseUser.getUid())&&chat.getSender().equals(userid)||
//                                chat.getReciver().equals(userid)&&chat.getSender().equals(firebaseUser.getUid())){
//                            lastMes =chat.getMessage();
//                        }
//                    }catch (Exception e) {
//
//                    }
//                }
//                switch (lastMes){
//                    case "default":
//                        laTextView.setText("...");
//                        break;
//                    default:
//                        laTextView.setText(lastMes);
//                        break;
//
//                }
//                lastMes="default";
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
//    }

}
